/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_CMDLINESPEC
#define INCLUDED_BUILDBOXCASD_CMDLINESPEC

#include <buildboxcommon_commandlinetypes.h>
#include <buildboxcommon_connectionoptions_commandline.h>

#include <string>
#include <vector>

namespace buildboxcasd {

struct CmdLineSpec {
    CmdLineSpec(
        const std::string &defaultLogLevel,
        const buildboxcommon::ConnectionOptionsCommandLine &casClientSpec,
        const buildboxcommon::ConnectionOptionsCommandLine &raClientSpec,
        const buildboxcommon::ConnectionOptionsCommandLine &acClientSpec);

    std::vector<buildboxcommon::CommandLineTypes::ArgumentSpec> d_spec;

    // positionals
    std::string d_cachePath;
};

} // namespace buildboxcasd

#endif
